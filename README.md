simple_budget_tracker
=====================

Tracks budget based on paydate and when bills are due, lets you know how much you can spend

Built with python3

Needs the prettytable python module
